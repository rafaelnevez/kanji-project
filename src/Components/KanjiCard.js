import React from 'react'
import { View, StyleSheet, Text, Dimensions,TouchableOpacity } from 'react-native'

export default props => {
    return (
        <TouchableOpacity style={styles.container} onPress={props.onShow}>
            <Text style={styles.text}>{props.kanji}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent:"center",
        backgroundColor: "white",
        borderWidth: 1,
        padding: 5,
        width:Dimensions.get("window").width - 100,
        height:150

    },
    text:{
        fontSize:80,
        textAlign:"center"
    }
})
