import React from 'react'
import { View, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native'
import params from '../Params'

export default props => {
    return (
        <View style={styles.container}>
            <TextInput style={styles.input}></TextInput>
            <TouchableOpacity onPress={() => props.showModal()}>
                <Image 
                    style={styles.buttonAdd}
                    source={require('../image/add_button.png')}
                />
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        backgroundColor: params.colors.redBasico,
        height: 50,
        borderWidth: 4,
        borderBottomColor: params.redQausePreto,
        borderColor: params.colors.redQausePreto,
    },
    input: {
        backgroundColor: params.colors.redQuaseBranco,
        marginLeft: 10,
        color: "white",
        fontSize: 20,
        flex: 2,
        marginLeft: 10,
        marginRight: 20,
    },
    buttonFilter: {
        flex: 1,
        height: 20,
        width: 20,
        padding: 5,
        marginRight: 5,
    },
    buttonAdd: {
        flex: 1,
        height: 30,
        width: 40,
        padding: 20
    },



});