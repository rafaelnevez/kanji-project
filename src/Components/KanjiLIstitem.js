import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, TouchableHighlight, Alert } from 'react-native'
import params from '../Params'




export const TrashButton = props => {
    return (
        <TouchableOpacity onPress={props.showItem}>
            <Image
                style={styles.buttonTrash}
                source={require('../image/trash_icon.png')}
            />
        </TouchableOpacity>
    )
}
export const EditButton = props => {
    return (
        <TouchableOpacity onPress={props.showItem}>
            <Image
                style={styles.buttonEdit}
                source={require('../image/edit_icon.png')}
            />
        </TouchableOpacity>
    )
}





export default KanjiListitem = props => {
    showItem = () => {
        Alert.alert(props.kanji+"" ,`Significado: ${props.significado}\nFrase: ${props.frase}\nBR: ${props.pt_BR}\n`)

    }
    return (
        <TouchableOpacity style={styles.container} onPress={this.showItem}>
            <Text style={styles.text} >{props.kanji} - {props.significado}</Text>
            <View style={styles.espacador} />
            <EditButton />
            <TrashButton />
        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "flex-start",
        backgroundColor: "white",
        alignItems: "center",
        padding: 5,
        marginBottom: 5,
        marginTop: 5,
        backgroundColor: params.colors.redBranco,
    },
    espacador: {
        flex: 1
    },
    text: {
        fontSize: 25,
        textAlign: "left",
        marginLeft: 10,
        color: "white"
    },
    buttonTrash: {
        height: 25,
        width: 20,
        marginRight: 10,
    },
    buttonEdit: {
        height: 25,
        width: 20,
        marginRight: 20,
    }
})