import React from 'react';
import { StyleSheet, View, ScrollView, FlatList,Alert } from 'react-native';
import KanjiListitem from './KanjiLIstitem'





const pegarKanjis = ({ item }) => {
    return <KanjiListitem {...item}/>
}


export default props => {
        return (
            <ScrollView style={[styles.container]}>
                <FlatList data={props.kanji} renderItem={pegarKanjis}
                    keyExtractor={(_, index) => index.toString()} />
            </ScrollView>
        )    
}
const styles = StyleSheet.create({
    container:{
        flex:1,   
        marginTop:0,     
    },
});