
import React, { Component } from 'react';
import {
  StyleSheet, Text, View, Button, Alert, NativeModules,
  LayoutAnimation, Dimensions
} from 'react-native';
import kanji from '../kanjis'
import  KanjiCard  from '../Components/KanjiCard'
//import { AsyncStorage } from 'react-native';

// EXTREMAMNTE IMPORTANTE P/ FUNCIONAMENTO NO ANDROID
const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);


export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = this.createState()
  }

  createState = () => {
    const atual = this.randomizar(kanji.length)
    for (let a = 0; a < kanji.length; a++) {
      kanji[a].lida = false
    }
    //  
    return {
      inicio: true,
      kanjis: kanji,
      atual,
      toShow: false,
      left: 0,
      cardPosition: Dimensions.get('window').width / 2.
    }
  }

  randomizar = (max) => {
    return parseInt(Math.random() * max, 10)
  }

  onProximoClicado = () => {
    const kanjisVetor = this.state.kanjis
    const atualAntigo = this.state.atual

    kanjisVetor[atualAntigo].lida = true
    const kanjis = kanjisVetor.filter(n => !n.lida)
    if (kanjis.length <= 0) {
      Alert.alert("ゲームオーバー!", " やった！！!!")
      this.setState(this.createState())
      return
    }
    const atual = this.randomizar(kanjis.length)


    LayoutAnimation.spring();

    this.setState({
      inicio: false,
      kanjis,
      atual,
      toShow: false,
      left: kanji.filter(n => n.lida).length
    })
  }




  onReiniciarClicado = () => {
    this.setState(this.createState())
  }

  onShow = () => {
    this.setState({ toShow: true })
    if (this.state.toShow) {
      this.onProximoClicado()
    }
  }

  /* teste = async () => {
    try {
      await AsyncStorage.setItem(this.state.kanjis[this.state.atual].kanji, JSON.stringify(this.state.kanjis[this.state.atual]));
    } catch (error) {
      Alert.alert("Erro!",error +"")
    }
  };

  retrive = async () => {
    try {
      const value = await AsyncStorage.getItem(this.state.kanjis[this.state.atual].kanji);
      if (value !== null) {
        Alert.alert("Erro!",JSON.stringify(value) + "")
      }
    } catch (error) {
      Alert.alert("Erro!","Erro ao salvar!")
    }
  }; */

  render() {

    return (
      <View style={styles.container}>
        <Text style={styles.countText}>{this.state.left}/{kanji.length}</Text>
        <Text style={[styles.significado, this.state.toShow ? {} : { color: "#F5FCFF" }]}>{this.state.kanjis[this.state.atual].significado}</Text>
        <KanjiCard onShow={this.onShow} style={styles.kanji} kanji={this.state.kanjis[this.state.atual].kanji}></KanjiCard>
        <Text style={styles.significado}>{this.state.kanjis[this.state.atual].frase}</Text>
        <Text style={[styles.significado, this.state.toShow ? {} : { color: "#F5FCFF" }]}>{this.state.kanjis[this.state.atual].pt_BR}</Text>
        <View style={styles.button}>
          <Button title="　もう以下愛　" color="red" style={styles.ButtonReini}
            onPress={this.onReiniciarClicado} />
          <View style={styles.separador} />
          <Button title="　　次　　" color="green" style={styles.ButtonProx}
            onPress={this.onProximoClicado} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    marginRight: 30,
    marginLeft: 30,
  },
  kanji: {
    marginBottom:30,
  },
  significado: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
    marginBottom: 15,
    marginTop: 20
  },
  frase: {
    textAlign: 'center',
    color: '#333333',
    fontSize: 90,
    marginTop: 50,
    marginBottom: 100
  },
  button: {
    justifyContent: 'space-around',
    flexDirection: "row",
    alignItems: 'center',
    marginLeft: 40,
    marginRight: 40,
    marginTop: 40
  },
  countText: {
    fontSize: 40,
  },
  buttonResp: {
  },
  ButtonProx: {
    flex: 1,
    width: 90,
  },
  ButtonReini: {
    flex: 1,
  },
  separador: {
    flex: 2,
  }
});
