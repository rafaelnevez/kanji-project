import React from 'react'
import { View, StyleSheet, TextInput, Button, Modal } from 'react-native'
import params from '../Params'
export default props => {
    return (
        <Modal onRequestClose={props.onCancel}
            visible={props.isVisible} animationType="fade"
            transparent={true}>
            <View style={styles.frame}>
                <View style={styles.container}>
                    <TextInput style={styles.input} placeholder="Kanji" />
                    <TextInput placeholder="Significado" />
                    <TextInput placeholder="Frase" />
                    <TextInput placeholder="pt_BR" />
                    <Button title="ADICIONAR" onPress={() => props.onAddKanji({
                        kanji: "TESTE",
                        significado: "けんきゅうし",
                        frase: "あには、大学でかがくを、研究しています。",
                        pt_BR: "Pesquisador",
                        lida: false,
                    })} />
                </View>

            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    frame: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: "#EEE",
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        borderRadius: 10,
        width: params.dimensions.screenWidth,
        borderColor: params.colors.redPreto,
        borderWidth: 3,
        marginRight: 20,
        marginLeft: 20,
    },
    input: {
        alignContent: "flex-start"
    }
})
