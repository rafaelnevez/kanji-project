import React, { Component } from 'react'
import { View, StyleSheet, Alert } from 'react-native'
import ListKanji from '../Components/ListKanji'
import kanji from '../kanjis'
import TopBar from '../Components/TopBar'
import AddModal from './AddModal'

export default class AddKanji extends Component {

    constructor(props) {
        super(props)
        this.state = this.createState()
    }

    createState = () => {
       Alert.alert("oi",kanji.length+"")
        return {
            listKanjis:kanji,
            isModalVisible: false
        }
    }


    onAddKanji = newKanji => {
        kanji.push(newKanji)
        this.setState(this.createState())
    }

    showModal = () => {
        this.setState({isModalVisible:true})
    }

    render() {
        return (
            <View style={styles.container}>
                <AddModal onAddKanji={this.onAddKanji} 
                isVisible={this.state.isModalVisible}
                onCancel={()=> this.setState({isModalVisible:false})}/>
                <TopBar showModal={this.showModal} />
                <ListKanji kanji={this.state.listKanjis} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",

    },
    topBar: {
        position: "absolute",
        top: 0,
        flex: 1,
        alignSelf: 'stretch',
        right: 0,
        left: 0,
    },
});