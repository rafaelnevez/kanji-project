import React from 'react'
import { createDrawerNavigator } from 'react-navigation'
import App from './App'
import AddKanji from './AddKanji'



export default createDrawerNavigator({
     AddKanji: {
        screen: () => <AddKanji></AddKanji>,
        title: "Adicionar Kanji"
    },
    Home: {
        screen: () => <App></App>,
        title: "Home"
    }, 

},
    {
        drawerWidth: 200,
        
    })



