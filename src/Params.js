import React from 'react'
import { Dimensions } from 'react-native'

export default params = {
    colors: {
        redPreto: "#7F0000",
        redQausePreto: "#7F2626",
        redBasico: "#CC0000",
        redQuaseBranco: "#FF0000",
        redBranco: "#FF4C4C"
    },
    dimensions:{
        screenWidth:Dimensions.get('window').width,
        screenHeight:Dimensions.get('window').height,
    }
}